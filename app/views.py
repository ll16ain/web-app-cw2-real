import os
from flask import render_template
from flask import flash, redirect, request, session, abort
from app import app
from app import db, models
from .forms import movieForm, finishForm, loginForm, signForm


@app.route('/task', methods=['GET', 'POST'])
def addTask():
    temp = session.get('userLog')
    if temp is not None:
        form = movieForm()
        if form.validate_on_submit():
            date = form.date.data
            date = date.strftime('%d-%m-%Y')
            tasker = models.movies(user=session.get('userLog'),title=form.title.data,description=form.description.data,date=form.date.data,rating=form.rating.data,watched=form.watched.data)
            db.session.add(tasker)
            db.session.commit()
            incTasks = models.movies.query.filter_by(user=session.get('userLog'))
            return render_template('allMovies.html', test=incTasks)
        else:
            return render_template('task.html', title='Add Task', form=form)
    else:
        return redirect('/')

@app.route('/unwatchedMovies')
def unwatchedMovies():
    temp = session.get('userLog')
    if temp is not None:
        incTasks = models.movies.query.filter_by(watched=False,user=session.get('userLog'))
        return render_template('unwatchedMovies.html', title="Unwatched Movies", test=incTasks)
    else:
        return redirect('/')

@app.route('/watchedMovies')
def watchedMovies():
    temp = session.get('userLog')
    if temp is not None:
        compTasks = models.movies.query.filter_by(watched=True,user=session.get('userLog'))
        return render_template('watchedMovies.html', title="Watched Movies", test=compTasks)
    else:
        return redirect('/')

@app.route('/allMovies')
def allMovies():
    temp = session.get('userLog')
    if temp is not None:
        everyTask = models.movies.query.filter_by(user=session.get('userLog'))
        return render_template('allMovies.html', title="Full List", test=everyTask)
    else:
        return redirect('/')

@app.route('/watchedMovie/<id>')
def watchedMovie(id):
    compTasks = models.movies.query.filter_by(watched=True,user=session.get('userLog'))
    taskToFinish = models.movies.query.filter_by(id=id,user=session.get('userLog')).first()
    taskToFinish.watched = True
    db.session.commit()
    return redirect('watchedMovies')

@app.route('/logout')
def logout():
    session.pop('userLog',None)
    return redirect('/')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    sigForm = signForm()
    logForm = loginForm()
    if sigForm.validate_on_submit():
        user = str(sigForm.username.data)
        passw = str(sigForm.password.data)
        confirPassw = str(sigForm.confirmPassword.data)
        if (passw != confirPassw):
            flash("Passwords must match")
            return render_template('signup.html', title="Signup", form=sigForm)
        else:
            newUser = models.users(user,passw)
            db.session.add(newUser)
            db.session.commit()
            return render_template('login.html', title='Login', form=sigForm)
    else:
        return render_template('signup.html', title="Signup", form=sigForm)
    return render_template('task.html', title='Add Task', form=form)


@app.route('/', methods=['GET', 'POST'])
def login():
        sigForm = signForm()
        logForm = loginForm()
        if logForm.validate_on_submit():
            temp = session.get('userLog')
            if temp is None:
                user = str(logForm.username.data)
                passw = str(logForm.password.data)
                dbUser = models.users.query.filter_by(username=user).first()
                if dbUser is not None:
                    if (str(dbUser.username) == user) and (str(dbUser.password) == passw):
                        session['userLog'] = user
                        return render_template('login.html', title="Login", form=logForm)
                else:
                    flash("No user found!")
                    return redirect('signup')
        return render_template('login.html', title="Login", form=logForm)
