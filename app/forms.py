from flask_wtf import Form
from wtforms import IntegerField, TextField, BooleanField, DateField, validators, RadioField, SelectField
from wtforms.validators import DataRequired, NumberRange, EqualTo

class movieForm(Form):
    title = TextField('Title', validators=[DataRequired()])
    description = TextField('Description', validators=[DataRequired()])
    date = DateField('Date',validators=[DataRequired()])
    rating = RadioField('Rating', choices=[('1','1. Terrible'),('2','2. Bad'),('3','3. Average'),('4','4. Good'),('5','5. Amazing')])
    watched = BooleanField('Watched')

class loginForm(Form):
    username = TextField('Username', validators=[DataRequired()])
    password = TextField('Password', validators=[DataRequired()])

class signForm(Form):
    username = TextField('Username', validators=[DataRequired()])
    password = TextField('Password', validators=[DataRequired()])
    confirmPassword = TextField('Confirm Password', validators=[DataRequired()])

class finishForm(Form):
    id = IntegerField('id')
